*** Settings ***
Variables         ../ElementsLocators/Variables.py
Variables         ../ElementsLocators/Object.py
Library           SeleniumLibrary

*** Keywords ***
LoginToApplication
    open browser    ${url}    ${browser}
    maximize browser window
    set browser implicit wait    5
    input text    ${usernamexpath}    ${username}
    input text    ${passwordxpath}    ${password}
    click button    ${loginbtnxpath}



VerifyMainApplicationText
    element text should be    ${ApplicationTitle}    ${AppTitle}
