*** Settings ***
Library           SeleniumLibrary
Variables         ../ElementsLocators/Variables.py
Variables         ../ElementsLocators/Object.py
Resource          ../Application/Login.robot


*** Test Cases ***
TC01_Login_TO_Swag_Labs
    LoginToApplication
    VerifyMainApplicationText
    capture page screenshot
    close browser